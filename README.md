# README #


*** Installation ***
1. With Clion
    a. Prerequisite
        - GCC Compiler
        - NCURSES Library
        - BashSupport Plugin for Clion
    b. Configurations
        There will be 2 run configs: poker & poker.sh:
            - "poker" will build the project. Set target to "All Target" and executable to "None".
            - "poker.sh" will run the terminal and start the game. Create a config as "Bash", script as the "poker.sh".
               Add "poker" config to the "Before Launch" panel.
    c. Run using the poker.sh config.

2. With CLI
    a. Prerequisite
        - GCC Compiler
        - NCURSES Library
    b. Compile using this command:
        [sudo] gcc -lcurses -o poker poker.c poker_ui.c save.c
    c. Run:
        ./poker
        
Poker rule: Our game plays exactly the same Texas Hold'em rule. However, all players will have the same money (100) and there
are no big blind and small blind in our game, therefore, the turn always start by Player 0 at the beginning of the game.

Target system: the program will be marked in the BIT2016A 64bit VM image. By default, we used Clion to develop our program

How to interact with our game: we chose to use UP-Key and Down-Key to select the menu options. In the game, the game will
be played by the user press "c" for calling/checking/by default it bets 2 money, "f" for folding the cards, "r" for raising
money by entering the amount of money that you want to bet. Finally, "q" for quitting the game and then the program will automatically
save the game, therefore, user can load into that game again. You can just save and load only 1 game at a time.

NOTE: if anything go wrong with the terminal, please go to preferences then choose the font to monospace and the size to 9.
Furthermore, you have to load the preset of solarized (dark) in colors option to see the best UI for our game.