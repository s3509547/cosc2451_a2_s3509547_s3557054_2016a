//
// Created by bit on 5/14/16.
//
#ifndef COSC2451_A2_S3509547_S3557054_2016A_SAVE_H
#define COSC2451_A2_S3509547_S3557054_2016A_SAVE_H

#include "poker.h"

typedef struct save {
    int end;
    int count;
    int index;
    int r;
    int pot;
    int playerCard;
    int riverCard;
    int choice;
    int total;
    int show;
    char str[101];
    Player p[4];
} Save;

/**
 * Save the game
 */
void save(Player p[], int end, int count, int index, int r, int pot, int playerCard, int riverCard, int total, int show, char str[101]);
/**
 * Load the game
 */
struct save load();


#endif //COSC2451_A2_S3509547_S3557054_2016A_SAVE_H
