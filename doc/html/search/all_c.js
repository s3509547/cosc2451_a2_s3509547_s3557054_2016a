var searchData=
[
  ['p',['p',['../structsave.html#a6eb20a6c25ccb763b359c4944593bfb1',1,'save']]],
  ['pick_5fmove',['pick_move',['../poker_8c.html#a362740a1433d282a4b44bd84e660b419',1,'pick_move(int smart, card hand[], card tablecard[], int riverCard, int *ai_r):&#160;poker.c'],['../poker_8h.html#a362740a1433d282a4b44bd84e660b419',1,'pick_move(int smart, card hand[], card tablecard[], int riverCard, int *ai_r):&#160;poker.c']]],
  ['player',['player',['../structplayer.html',1,'player'],['../poker_8h.html#ac6f795d0d2e88ee469ddc704329e7cc3',1,'Player():&#160;poker.h']]],
  ['playercard',['playerCard',['../structsave.html#a91feef4a3d4089c49b5e1d8ebff83df7',1,'save']]],
  ['playernum',['playerNum',['../poker__ui_8c.html#a0ae8a274f7d0f338ba0809389c2c427a',1,'poker_ui.c']]],
  ['poker_2ec',['poker.c',['../poker_8c.html',1,'']]],
  ['poker_2eh',['poker.h',['../poker_8h.html',1,'']]],
  ['poker_5fui_2ec',['poker_ui.c',['../poker__ui_8c.html',1,'']]],
  ['poker_5fui_2eh',['poker_ui.h',['../poker__ui_8h.html',1,'']]],
  ['pot',['pot',['../structsave.html#aac877711d8d5d1bc8108a33b6bf35976',1,'save']]],
  ['printcard',['printCard',['../poker__ui_8c.html#a755eddde4aab426835f2921e6cc2c5ed',1,'printCard(WINDOW *wn_a, int length, card hand[length], int location, int eval, int index, Player p[]):&#160;poker_ui.c'],['../poker__ui_8h.html#af08fb757670e882be766a827e6d0b9df',1,'printCard(WINDOW *wn_a, int length, card hand[length], int location, int eval, int index, Player *p):&#160;poker_ui.h']]]
];
