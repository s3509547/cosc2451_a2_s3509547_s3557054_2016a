var searchData=
[
  ['save',['save',['../save_8c.html#aea6cf7ba3fe48036ed61fbd0b7f375fc',1,'save(Player p[], int end, int count, int index, int r, int pot, int playerCard, int riverCard, int total, int show, char str[101]):&#160;save.c'],['../save_8h.html#aea6cf7ba3fe48036ed61fbd0b7f375fc',1,'save(Player p[], int end, int count, int index, int r, int pot, int playerCard, int riverCard, int total, int show, char str[101]):&#160;save.c']]],
  ['selection_5fsort',['selection_sort',['../poker_8c.html#a98f933bed94bc9454fb97f673bcc8945',1,'selection_sort(Player a[], int length):&#160;poker.c'],['../poker_8h.html#a98f933bed94bc9454fb97f673bcc8945',1,'selection_sort(Player a[], int length):&#160;poker.c']]],
  ['shuffle',['shuffle',['../poker_8c.html#a161a6cb64d4eeecbb848cca4a01093e8',1,'shuffle(card deck[]):&#160;poker.c'],['../poker_8h.html#a161a6cb64d4eeecbb848cca4a01093e8',1,'shuffle(card deck[]):&#160;poker.c']]],
  ['swap_5fcard',['swap_card',['../poker_8c.html#a3faffc971927cb33464ca3303cf680bd',1,'swap_card(card deck[], int dest, int src):&#160;poker.c'],['../poker_8h.html#a3faffc971927cb33464ca3303cf680bd',1,'swap_card(card deck[], int dest, int src):&#160;poker.c']]],
  ['swap_5fplayer',['swap_player',['../poker_8c.html#a29748c1793925585bf0c473ef0300ff3',1,'swap_player(Player *p1, Player *p2):&#160;poker.c'],['../poker_8h.html#a29748c1793925585bf0c473ef0300ff3',1,'swap_player(Player *p1, Player *p2):&#160;poker.c']]]
];
