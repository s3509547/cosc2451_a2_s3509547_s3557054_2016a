var searchData=
[
  ['save',['save',['../structsave.html',1,'save'],['../save_8h.html#ae3b4a2b97aa606adfc7ccb99c72db0e2',1,'Save():&#160;save.h'],['../save_8c.html#aea6cf7ba3fe48036ed61fbd0b7f375fc',1,'save(Player p[], int end, int count, int index, int r, int pot, int playerCard, int riverCard, int total, int show, char str[101]):&#160;save.c'],['../save_8h.html#aea6cf7ba3fe48036ed61fbd0b7f375fc',1,'save(Player p[], int end, int count, int index, int r, int pot, int playerCard, int riverCard, int total, int show, char str[101]):&#160;save.c']]],
  ['save_2ec',['save.c',['../save_8c.html',1,'']]],
  ['save_2eh',['save.h',['../save_8h.html',1,'']]],
  ['selection_5fsort',['selection_sort',['../poker_8c.html#a98f933bed94bc9454fb97f673bcc8945',1,'selection_sort(Player a[], int length):&#160;poker.c'],['../poker_8h.html#a98f933bed94bc9454fb97f673bcc8945',1,'selection_sort(Player a[], int length):&#160;poker.c']]],
  ['show',['show',['../structsave.html#af74a57ccdc442135a475f8efbf4c19d8',1,'save']]],
  ['shuffle',['shuffle',['../poker_8c.html#a161a6cb64d4eeecbb848cca4a01093e8',1,'shuffle(card deck[]):&#160;poker.c'],['../poker_8h.html#a161a6cb64d4eeecbb848cca4a01093e8',1,'shuffle(card deck[]):&#160;poker.c']]],
  ['start',['start',['../poker__ui_8c.html#a37722a150250e2a5a98e5e0d11e53449',1,'poker_ui.c']]],
  ['str',['str',['../structsave.html#ac96f573757a07b50076463c12f64bb43',1,'save']]],
  ['suit',['suit',['../poker_8h.html#a86f5b28278b0c53d86175aa38ca79e62',1,'poker.h']]],
  ['swap_5fcard',['swap_card',['../poker_8c.html#a3faffc971927cb33464ca3303cf680bd',1,'swap_card(card deck[], int dest, int src):&#160;poker.c'],['../poker_8h.html#a3faffc971927cb33464ca3303cf680bd',1,'swap_card(card deck[], int dest, int src):&#160;poker.c']]],
  ['swap_5fplayer',['swap_player',['../poker_8c.html#a29748c1793925585bf0c473ef0300ff3',1,'swap_player(Player *p1, Player *p2):&#160;poker.c'],['../poker_8h.html#a29748c1793925585bf0c473ef0300ff3',1,'swap_player(Player *p1, Player *p2):&#160;poker.c']]]
];
