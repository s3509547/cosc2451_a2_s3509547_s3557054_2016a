var searchData=
[
  ['hand',['hand',['../structplayer.html#ad24ec83af2dfa225eba98335785faa01',1,'player']]],
  ['hand_5fname',['hand_name',['../poker_8c.html#a0fa96771d892b7d36a9ebbcaf280c7ed',1,'hand_name(int in_rank):&#160;poker.c'],['../poker_8h.html#a0fa96771d892b7d36a9ebbcaf280c7ed',1,'hand_name(int in_rank):&#160;poker.c']]],
  ['hand_5frank',['hand_rank',['../poker_8c.html#a3d4ab66e25c4c86aad2b1ba0d8b34bfe',1,'hand_rank(const card *in_cards, int in_count):&#160;poker.c'],['../poker_8h.html#a3d4ab66e25c4c86aad2b1ba0d8b34bfe',1,'hand_rank(const card *in_cards, int in_count):&#160;poker.c']]],
  ['hand_5ftype',['hand_type',['../poker_8c.html#a07a61e2abca245b8a662e879ba795828',1,'poker.c']]]
];
