var searchData=
[
  ['readme',['README',['../md_README.html',1,'']]],
  ['r',['r',['../structsave.html#ab6d658a294d946b93ffe7a5f02fcdef3',1,'save']]],
  ['raise',['raise',['../poker_8c.html#a4fafef6c132cb33f6401a011c5a17990',1,'raise(Player *p, int *pot, int money):&#160;poker.c'],['../poker_8h.html#a4fafef6c132cb33f6401a011c5a17990',1,'raise(Player *p, int *pot, int money):&#160;poker.c']]],
  ['raised',['raised',['../structplayer.html#a20281ca04757c4e7103b7ddaf1287f53',1,'player']]],
  ['rank',['rank',['../structplayer.html#aedb7851e6ab762873b8e6dac374b4ddc',1,'player']]],
  ['readme_2emd',['README.md',['../README_8md.html',1,'']]],
  ['reset_5fcall',['reset_call',['../poker_8c.html#af2a248c9ac527e4928551705b17486d1',1,'reset_call(Player p[]):&#160;poker.c'],['../poker_8h.html#af2a248c9ac527e4928551705b17486d1',1,'reset_call(Player p[]):&#160;poker.c']]],
  ['reset_5fraised',['reset_raised',['../poker_8c.html#a58e3ec2193c775e3680ac7488f8998ca',1,'reset_raised(Player p[]):&#160;poker.c'],['../poker_8h.html#a58e3ec2193c775e3680ac7488f8998ca',1,'reset_raised(Player p[]):&#160;poker.c']]],
  ['rivercard',['riverCard',['../structsave.html#a66de59dc2cfb40a9342f631350c03330',1,'save']]]
];
