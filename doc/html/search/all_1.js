var searchData=
[
  ['call',['call',['../structplayer.html#a7ceae0696c23cdf2203f1e0346f1ba50',1,'player::call()'],['../poker_8c.html#ac2411b853b1849d37514239e56fb79df',1,'call(Player *p, int pot):&#160;poker.c'],['../poker_8h.html#ac2411b853b1849d37514239e56fb79df',1,'call(Player *p, int pot):&#160;poker.c']]],
  ['card',['card',['../poker_8h.html#ab16a632e2e3f1f154cce75648902eb83',1,'poker.h']]],
  ['check_5fgame_5fstate',['check_game_state',['../poker__ui_8c.html#a91ae3ffb37b87d8db25d1c7a0d3fdd91',1,'check_game_state(int *end, int r, int count):&#160;poker_ui.c'],['../poker__ui_8h.html#a91ae3ffb37b87d8db25d1c7a0d3fdd91',1,'check_game_state(int *end, int r, int count):&#160;poker_ui.c']]],
  ['choice',['choice',['../structsave.html#af0f4fee0666497d718de27b6c0e31421',1,'save']]],
  ['chooseplayernum',['choosePlayerNum',['../poker__ui_8c.html#ace276149e91aebff7b63cbba14428fdf',1,'choosePlayerNum(WINDOW *wnPNum):&#160;poker_ui.c'],['../poker__ui_8h.html#ace276149e91aebff7b63cbba14428fdf',1,'choosePlayerNum(WINDOW *wnPNum):&#160;poker_ui.c']]],
  ['clearwn',['clearWn',['../poker__ui_8c.html#a7c61bace506dd59dd25474928340dd27',1,'clearWn(WINDOW *wn):&#160;poker_ui.c'],['../poker__ui_8h.html#a7c61bace506dd59dd25474928340dd27',1,'clearWn(WINDOW *wn):&#160;poker_ui.c']]],
  ['count',['count',['../structsave.html#a43aceb92084b5b7c16873a8f9e7b0338',1,'save']]]
];
