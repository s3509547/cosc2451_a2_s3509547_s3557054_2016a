#ifndef COSC2451_A2_S3509547_S3557054_2016A_POKER_H_H
#define COSC2451_A2_S3509547_S3557054_2016A_POKER_H_H

#define N_CARDS  52
#define face( _in_card )	( ( _in_card ) % 13 )		
#define suit( _in_card )	( ( _in_card ) / 13 )		

typedef unsigned char card;

typedef struct player {
    int bet;
    int money;
    int rank;
    int fold;
    int raised;
    int call;
    char name[2];
    card hand[7];
} Player;

/**
 * Return the character of face of each card
 * @param in_card
 */
char face_letter(card in_card );

/**
 * Initialize the deck of cards
 * @param deck[] is the deck of cards
 */
void init_card(card deck[]);

/**
 * Shuffle the deck of cards
 * @param deck[] is the deck of cards
 */
void shuffle(card deck[]);

/**
* Takes 2 integers representing cards and swap theirs value in deck of cards
* @param deck[] is the arrays of cards or deck of cards
* @param dest is the integer
* @param src is the integer
*/
void swap_card (card deck[], int dest, int src);

/**
* Takes 2 struct pointers and swap theirs value/positions to set whose hand rank is higher
* @param p1 is the struct pointer
* @param p2 is the struct pointer
*/
void swap_player(Player *p1, Player *p2);

/** Sort an int array using the selection sort algorithm.
  * @param a the int array to be sorted in place
  * @param length the length of array a
  */
void selection_sort( Player a[], int length);

/** Return the name of poker hand ranking
  * @param in_rank the hand's score.
  */
const char *hand_name( int in_rank );

/**
 * Calculate the score of the given hand.
 * @param in_card pointer to the cards in hand.
 * @param in_count number of cards to eval.
 */
int hand_rank( const card *in_cards, int in_count );

/**
  * Function for player to call money that matches to the current bet
  * @param p is the player pointer that point to each player
  * @param pot is the integer pointer that is the amount of money user call
  */

void call(Player *p, int pot);

/**
  * Fold cards of player and keep count the player's turn
  * @param p is the player pointer that points to each player
  * @param count is the integer pointer that counts the number of players
  */
void fold(Player *p, int *count);

/**
  * Raise money
  * @param p is the player pointer that points to each player
  * @param pot is the pointer to integer
  * @param money is the integer
  */
int raise(Player *p, int *pot, int money);

/**
  * Initialize players, set players' cards and set table cards to players
  * @param player_list[] is the array of players
  * @param n_player is the integer represents number of players
  * @param deck[]
  * @param table_card[]
  */
void init_player(Player player_list[], int n_player, card deck[], card table_card[]);

/**
  * Initialize all players' name and set money to each player
  * @param player_list[] is the array of players
  * @param n_players is the integer represents number of players
  */
void init_name_money(Player player_list[], int n_player);

/**
  * Reset call of all players
  */
void reset_call(Player p[]);

/**
  * Reset raised of all players
  */
void reset_raised(Player p[]);

/**
 * Pick a move for AI.
 */
char pick_move(int smart, card hand[], card tablecard[], int riverCard, int *ai_r);

#endif //COSC2451_A2_S3509547_S3557054_2016A_POKER_H_H
