#ifndef COSC2451_A2_S3509547_S3557054_2016A_POKER_UI_H
#define COSC2451_A2_S3509547_S3557054_2016A_POKER_UI_H
#include <ncurses.h>
#include "save.h"
/** 
* Return the integer type 
*/
int digit_to_int(char d);

/* Clear the active window.
*
* @param wn The active window to clear.
*/
void clearWn(WINDOW *wn);

/**
* Choose amount of players.
*/
void choosePlayerNum(WINDOW *wnPNum);

/**
* View the credit.
*/
void viewCredit(WINDOW *wn_a);

/**
* Print cards
* @param wn_a is the selected window to print card
* @param length is the number of cards will be printed out
* @param hand[length] is the array of cards of each player
* @param location is the integer that set where the card will be printed out
* @param eval is the integer that show cards or not
* @param index is the integer 
* @param p is the player pointer that points to each player
*/
void printCard(WINDOW *wn_a, int length, card hand[length], int location, int eval, int index, Player *p);

/**
* Check the game state of the game
*/
int check_game_state(int *end, int r, int count);

/**
* The game menu
*/
void game(WINDOW* wn, Player player_list[], card table_card[], int *quit, struct save s, int loaded);

/**
 * Menu manager.
 * @param wn The menu window.
 */
void drawMenu(WINDOW* wn_a);

#endif //COSC2451_A2_S3509547_S3557054_2016A_POKER_UI_H
