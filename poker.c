#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "poker.h"
enum hand_type {
    /* poker hands are encoded as 24-bit integers */

            k_high_card =	0x00000000	,	/* high card is			0x000-----	[high] [2nd high] [3rd high] [4th high] [low] */
            k_pair =	0x00400000	,	/* pair is				0x0040----	[pair] [high] [2nd high] [low] */
            k_two_pair =	0x00500000	,	/* two pair is			0x0050--0-	[pair] [2nd pair] [high] */
            k_three_of_a_kind	=	0x00800000	,	/* three of a kind is	0x008-00--	[3-kind] [high] [low] */
            k_straight =	0x00900000	,	/* straight is			0x009-----	[high] [2nd high] [3rd high] [4th high] [low] */
            k_flush =	0x00a00000	,	/* flush is				0x00a-----	[high] [2nd high] [3rd high] [4th high] [low] */
            k_full_house =	0x00c00000	,	/* full house is		0x00c--000	[3-kind] [pair] */
            k_four_of_a_kind	=	0x00d00000	,	/* four of a kind is	0x00d-000-	[4-kind] [high] */
            k_straight_flush	=	0x00e00000	,	/* straight flush is	0x00e-----	[high] [2nd high] [3rd high] [4th high] [low] */

            k_hand_type_mask	=	0x00f00000
};

void swap_card (card deck[], int dest, int src) {
    char temp;
    temp = deck[src];
    deck[src] = deck[dest];
    deck[dest] = temp;
}

void swap_player(Player *p1, Player *p2) {
    Player temp = *p1;
    *p1 = *p2;
    *p2 = temp;
}

void selection_sort(Player a[], int length) {
    int min_pos;

    /* find the min value in a[i] to a[length-1], and swap it with a[i] */
    for (int i = 0; i < length; i++) {
        min_pos = i;
        /* find the minimum value in what is left of the array */
        for (int j = i + 1; j < length; j++) {

            if (a[j].rank < a[min_pos].rank) {
                min_pos = j;
            }
        }
        swap_player(&a[i], &a[min_pos]);
    }
}

void shuffle(card deck[]){
    int src, dest;
    for (int i = 0; i < N_CARDS; i++) {
        src = i;
        dest = rand() % N_CARDS;
        swap_card(deck,src,dest);
    }
}

char face_letter( card in_card ) {
    switch ( face( in_card ) ) {
        case 0:						return '2';
        case 1:						return '3';
        case 2:						return '4';
        case 3:						return '5';
        case 4:						return '6';
        case 5:						return '7';
        case 6:						return '8';
        case 7:						return '9';
        case 8:						return 'T';
        case 9:						return 'J';
        case 10:					return 'Q';
        case 11:					return 'K';
        case 12:					return 'A';
    }

    return '?';
}

const char *hand_name( int in_rank ) {
    const char			   *result;

    switch ( in_rank & k_hand_type_mask ) {
        case k_high_card:
            return "High card";
        case k_pair:
            return "Pair";
        case k_two_pair:
            return "Two pair";
        case k_three_of_a_kind:
            return "Three of a kind";
        case k_straight:
            return "Straight";
        case k_flush:
            return "Flush";
        case k_full_house:
            return "Full house";
        case k_four_of_a_kind:
            return "Four of a kind";
        case k_straight_flush:
            return "Straight flush";
    }

    return (char *) 0;
}


/* assumes in_cards contains in_count cards wide where 5 <= in_count <= 7 */
int hand_rank( const card *in_cards, int in_count ) {
    unsigned char faces[ 5 ][ 15 ], suits[ 4 ] = { 0, 0, 0, 0 };
    int	i, j, k, mask, n, result = 0, s = 0;

    memset( faces, 0, sizeof(faces) );

    /* search for flush */
    for ( i = 0; i < in_count; ++i ) {
        j = face( in_cards[ i ] );
        k = suit( in_cards[ i ] );

        if ( ++suits[ k ] == 5 ) {
            result = k_flush;
            s = k + 1;
        }

        ++faces[ 0 ][ j + 2 ];
        ++faces[ k + 1 ][ j + 2 ];
    }

    /* search for straight [flush] */
    for ( i = 0; i < 2; ++i, s = 0 ) {
        if ( i ^ ! ( result & k_flush ) )
            continue;

        faces[ s ][ 1 ] = faces[ s ][ 14 ];

        for ( j = 15; j-- > 5; ) {
            if ( faces[ s ][ j ] ) {
                if ( faces[ s ][ j - 1 ] ) {
                    if ( faces[ s ][ j - 2 ] ) {
                        if ( faces[ s ][ j - 3 ] ) {
                            if ( faces[ s ][ j - 4 ] ) {
                                k = j << 16 | (j - 1) << 12 | (j - 2) << 8 | (j - 3) << 4 | (j - 4);

                                return ( s ? k_straight_flush : k_straight ) | k;
                            } else j -= 4;
                        } else j -= 3;
                    } else j -= 2;
                } else j -= 1;
            }
        }
    }

    /* no straight or flush, search for other hands */
    for ( i = 0, j = 15, k = 0; i < in_count; i += n ) {
        while ( ! ( n = faces[ 0 ][ --j ] ) ) ;

        switch ( n ) {
            case 1:
                k = k << 4 | j;
                break;

            case 2: {
                switch ( result & k_hand_type_mask ) {
                    case k_three_of_a_kind:
                        result |= k_full_house | j << 12;
                        return result;
                    case k_four_of_a_kind:													/* fall through */
                    case k_flush:															/* fall through */
                    case k_two_pair:
                        k = k << 4 | j;
                        break;
                    case k_pair:
                        result |= k_two_pair | j << 8;
                        break;
                    case k_high_card:
                        result = k_pair | j << 12;
                        break;
                }
            }
                break;

            case 3: {
                switch ( result & k_hand_type_mask ) {
                    case k_four_of_a_kind:													/* fall through */
                    case k_flush:
                        k = k << 4 | j;
                        break;
                    case k_three_of_a_kind:
                        result |= k_full_house | j << 12;
                        return result;
                    case k_two_pair:
                        result &= 0xf000;							/* fall through */
                    case k_pair:
                        result |= k_full_house | j << 16;
                        return result;
                    case k_high_card:
                        result = k_three_of_a_kind | j << 16;
                        break;
                }
            }
                break;

            case 4:
                result = k_four_of_a_kind | j << 16;
                break;
        }
    }

    switch ( result & k_hand_type_mask ) {
        case k_four_of_a_kind:																/* fall through */
        case k_two_pair:
            mask = 0xf0;
            break;
        case k_three_of_a_kind:
            mask = 0xf00;
            break;
        case k_pair:
            mask = 0xf000;
            break;
        case k_flush:																		/* fall through */
        case k_high_card:
            mask = 0xf00000;
            break;
    }

    while ( k & mask ) k >>= 4;

    return result | k;
}

void call(Player *p, int pot) {

    if (p->money < (pot - p->bet)) {
        p->money = 0;
    } else if (p->money == 0) {
        p->call = 1;
        return;
    } else if (p->bet == pot) {
        p->call = 1;
        return;
    } else {
        p->money -= (pot - p->bet);
    }
    p->call = 1;
}

void fold(Player *p, int *count) {
    p->fold = 1;
    *count += 1;
}

int raise(Player *p, int *pot, int money) {

    if (p->money < money)
        return 0;
    if (money <= *pot)
        return 0;

    p->money -= money;
    p->bet = money;
    *pot = money;
    return 1;
}

void init_card(card deck[]) {

    for (int a = 0; a < N_CARDS; a++)
        deck[ a ] = (card) a;

    shuffle(deck);
}

void init_name_money(Player player_list[], int n_player) {
    for (int a = 0; a < n_player; a++) {
        sprintf(player_list[a].name, "%d", a);
        player_list[a].money = 100;
    }
}

void init_player(Player player_list[], int n_player, card deck[], card table_card[]) {

    int bar = 0;
    int count = 0;

    for (int a = 0; a < n_player; a++) {
        player_list[a].fold = 0;
        player_list[a].bet = 0;
        player_list[a].call = 0;
        player_list[a].raised = 0;
    }

    while (bar < 2) {
        for (int a = 0; a < n_player; a++) {
            if (!player_list[a].money)
                continue;
            player_list[a].hand[bar] = deck[count];
            count++;
        }
        bar++;
    }

    count++;
    for (int b = 0; b < 3; b++) {
        table_card[b] = deck[count];
        count++;
    }
    count++;
    for (int b = 3; b < 5; b++) {
        table_card[b] = deck[count];
        count += 2;
    }
}
void reset_call(Player p[]) {

    for (int a = 0; a < 4; a++)
        p[a].call = 0;
}

void reset_raised(Player p[]) {

    for (int a = 0; a < 4; a++)
        p[a].raised = 0;
}

char pick_move(int smart, card hand[], card tablecard[], int riverCard, int *ai_r) {

    card dump[2 + riverCard];
    dump[0] = hand[0];
    dump[1] = hand[1];

    for (int a = 2; a < 2 + riverCard; a++)
        dump[a] = tablecard[a - 2];

    int type = hand_rank(dump, riverCard + 2) & k_hand_type_mask, r;
    char c[4] = "cfr";

    switch(smart) {
        case 0:
            r = rand() % 2;
            if (r == 2)
                *ai_r = rand() % 50;
            return c[r];
        case 1:
            if (type < k_two_pair)
                return 'f';
            return 'c';
        case 2:
            if ((type == k_high_card) && riverCard >= 3)
                return 'f';
            if ((type < k_two_pair) && riverCard >= 3)
                return 'c';
            if ((type >= k_two_pair) && riverCard >= 3) {
                *ai_r = rand() % 50;
                return 'r';
            }
            return 'c';

    }

}