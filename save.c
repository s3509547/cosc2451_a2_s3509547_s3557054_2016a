#include <stdio.h> // fopen(), fclose(), fscanf()
#include "save.h"

struct save load() {

    FILE *fd;

    struct save save;

    fd = fopen("saves/save.txt", "r");

    for (int a = 0; a < 4; a++) {
        Player *p = &save.p[a];
        fscanf(fd, "%d %d %d %d %d %d %s ", &p->bet, &p->money, &p->rank, &p->fold, &p->raised, &p->call, p->name);
        for (int b = 0; b < 7; b++) {
            fscanf(fd, "%i ", (int *)&save.p[a].hand[b]);
        }

    }

    fscanf(fd, "%i %i %i %i %i %i %i %i %i %s", &save.end, &save.count, &save.index, &save.r, &save.pot, &save.playerCard, &save.riverCard, &save.total, &save.show, save.str);

    fclose(fd);
    return save;
}

void save(Player p[], int end, int count, int index, int r, int pot, int playerCard, int riverCard, int total, int show, char str[101]) {

    FILE *fd;

    fd = fopen("saves/save.txt", "w+");

    for (int a = 0; a < 4; a++) {
        fprintf(fd, "%i %i %i %i %i %i %s ", p[a].bet, p[a].money, p[a].rank, p[a].fold, p[a].raised, p[a].call, p[a].name);
        for (int b = 0; b < 7; b++)
            fprintf(fd, "%i ", p[a].hand[b]);
        fprintf(fd, "\n");
    }

    fprintf(fd, "%i %i %i %i %i %i %i %i %i %s\n", end, count, index, r, pot, playerCard, riverCard, total, show, str);

    fclose(fd);
}
