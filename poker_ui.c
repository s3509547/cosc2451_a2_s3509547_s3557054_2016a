#include <ncurses.h>
#include <stdlib.h>
#include <time.h>
#include "poker.h"
#include "poker_ui.h"
#include <unistd.h>

char *menuOpt[4] = {"New game", "Load", "Credits", "Quit Game"};
char *member[6] = {"COSC2451 - Programming Techniques(2016A)",
                   "Lecturer's name: Denis Rinfret",
                   "Name: Nguyen Trung Dang Thuy",
                   "ID: s3509547",
                   "Name: Nguyen Sieu Bo",
                   "ID: s3557054"};

int start = 0;
int playerNum;

int digit_to_int(char d) {
    char str[2];

    str[0] = d;
    str[1] = '\0';
    return (int) strtol(str, NULL, 10);
}

void clearWn(WINDOW* wn) {
    wgetch(wn);
    werase(wn);
    mvwprintw(wn, 4, 75, "Press any key...");
    wgetch(wn);
    werase(wn);
}

void choosePlayerNum(WINDOW * wnPNum) {
    mvwprintw(wnPNum, 5, 30, "Recommendation: please play in full screen.\n");
    mvwprintw(wnPNum, 4, 30, "Enter player number(default 2 - maximum 4): ");
    playerNum = digit_to_int(wgetch(wnPNum));
    switch(playerNum) {
        case 2:
        case 3:
        case 4:
            break;
        default:
            playerNum = 4;
            break;
    }
    werase(wnPNum);
    endwin();
}

void viewCredit(WINDOW *wn_a) {
    werase(wn_a);
    int x = 4;
    for (int i = 0; i < 6; i++) {
        move(x,75);
        printw("%s\n", member[i]);
        x++;
    }
    clearWn(wn_a);
}

void printCard(WINDOW *wn_a, int length, card hand[length], int location, int eval, int index, Player p[]) {

    int x, y;
    start_color();
    init_pair(1,COLOR_BLACK, COLOR_WHITE);
    init_pair(2,COLOR_RED,COLOR_WHITE);
    init_pair(3, 0 ,COLOR_WHITE);

    switch(location) {
        case 0:
            y = 25;
            x = 80;
            break;
        case 1:
            y = 15;
            x = 25;
            break;
        case 2:
            y = 5;
            x = 80;
            break;
        case 3:
            y = 15;
            x = 145;
            break;
        case 4:
            y = 40;
            x = 75;
            break;
    }

    if (location == index) {
        mvprintw(y - 1, x, "Player: %i. Money: %i.  *", location, p[location].money);
    } else if (location == 4) {
        mvprintw(y - 1, x, "Community cards.");
    } else {
        mvprintw(y - 1, x, "Player: %i. Money: %i.", location, p[location].money);
    }

    move(y,x);
    for (int i = 0; i < length; i++){
        wattrset(wn_a,COLOR_PAIR(3));
        printw(".______");
    }
    printw(".\n");
    y++;
    move(y,x);
    for (int i = 0 ;i < length; i++){
        wattrset(wn_a,COLOR_PAIR(3));
        printw("|");
        if ((location != 0) && !eval) {
            printw("------");
        } else {
            printw("%c", face_letter(hand[i]));

            if (suit(hand[i]) == 0){
                wattrset(wn_a,COLOR_PAIR(1));
                printw("  .  ");
            } else if (suit(hand[i]) == 1){
                wattrset(wn_a,COLOR_PAIR(1));
                printw("  _  ");
            } else if (suit(hand[i]) == 2) {
                wattrset(wn_a,COLOR_PAIR(2));
                printw(" /\\  ");
            } else if (suit(hand[i]) == 3){
                wattrset(wn_a,COLOR_PAIR(2));
                printw("_  _ ");
            }
        }
        if (i == length -1){
            printw( "|\n");
        }
    }

    y++;
    move(y,x);
    for (int i = 0; i < length; i++) {
        printw("|");
        if ((location != 0) && !eval) {
            printw("------");
        } else {
            if (suit(hand[i]) == 0) {
                wattrset(wn_a,COLOR_PAIR(1));
                printw("  / \\ ");
            } else if (suit(hand[i]) == 1) {
                wattrset(wn_a,COLOR_PAIR(1));
                printw("  ( ) ");
            } else if (suit(hand[i]) == 2) {
                wattrset(wn_a,COLOR_PAIR(2));
                printw(" /  \\ ");
            } else if (suit(hand[i]) == 3) {
                wattrset(wn_a,COLOR_PAIR(2));
                printw("( \\/ )");
            }
        }
        if (i == length - 1) {
            attrset(COLOR_PAIR(1));
            printw("|\n");
        }
    }

    y++;
    move(y,x);
    for (int i = 0; i < length; i++) {
        printw("|");
        if ((location != 0) && !eval) {
            printw("------");
        } else {
            if (suit(hand[i]) == 0) {
                wattrset(wn_a,COLOR_PAIR(1));
                printw(" (_,_)");
            } else if (suit(hand[i]) == 1) {
                wattrset(wn_a,COLOR_PAIR(1));
                printw(" (_x_)");
            } else if (suit(hand[i]) == 2) {
                wattrset(wn_a,COLOR_PAIR(2));
                printw(" \\  / ");
            } else if (suit(hand[i]) == 3){
                wattrset(wn_a,COLOR_PAIR(2));
                printw(" \\  / ");
            }
        }

        if (i == length - 1) {
            attrset(COLOR_PAIR(1));
            printw("|\n");
        }
    }

    y++;
    move(y,x);
    for (int i = 0; i < length; i++){
        printw("|");
        if ((location != 0) && !eval) {
            printw("------");
        } else {
            if (suit(hand[i]) <= 1) {
                wattrset(wn_a,COLOR_PAIR(1));
                printw("   I  ");
            } else if(suit(hand[i]) <= 3) {
                wattrset(wn_a,COLOR_PAIR(2));
                printw("  \\/  ");
            }
        }
        if (i == length - 1) {
            attrset(COLOR_PAIR(1));
            printw("|\n");
        }
    }

    y++;
    move(y,x);
    for (int i = 0; i < length; i++){
        if (i == 0){
            printw("'------");
        } else {
            printw("^------");
        }
        if ( i == length -1){
            printw("'\n");
        }


    }
    attroff(COLOR_PAIR(1)| COLOR_PAIR(2));
}

int check_game_state(int *end, int r, int count) {

    /* Evaluate hand when enough cards were dealt */
    if (r > 3) {
        *end = 1;
        return 1;
    }

    /* Count remaining players */
    if (count == (playerNum - 1)) {
        *end = 1;
        return 2;
    }

    return 0;
}

void game(WINDOW* wn, Player player_list[], card table_card[], int * quit, struct save s, int loaded) {

    int end = 0, count = 0, index = 0, r = 0, pot = 2,  playerCard = 2, riverCard = 0, choice = 0, total = 0, show = 1, ai_r = 0, foo = 0;
    char str[101];

    if (loaded) {
        player_list = s.p;
        end = s.end;
        count = s.count;
        index = s.index;
        r = s.r;
        pot = s.pot;
        playerCard = s.playerCard;
        riverCard = s.riverCard;
        total = s.total;
        show = s.show;
    }

    while (!end) {

        if (check_game_state(&end, r, count)) {
            break;
        }

        while(!player_list[index].call) {

            printw("Total: %i\n", total);

            if (check_game_state(&end, r, count) == 2) {
                show = 0;
                break;
            }

            if (player_list[index].fold) {
                index = (index < (playerNum - 1)) ? index + 1 : 0;
                continue;
            }

            if (r > 0)
                printCard(wn, riverCard, table_card, 4, 1, -1, NULL);

            for (int b = 0; b < playerNum; b++)
                printCard(wn, playerCard, player_list[b].hand, b, 0, index, player_list);

            ch:
            if (index == 0) {
                choice = wgetch(wn);
            } else {
                choice = pick_move(rand() % 2, player_list[index].hand, table_card, riverCard, &ai_r);
            }

            switch (choice) {
                case 'c':
                    call(&player_list[index], pot);
                    total += (pot - player_list[index].bet);
                    break;
                case 'f':
                    fold(&player_list[index], &count);
                    break;
                case 'r':

                    if (index == 0) {
                        printw("Enter amount: ");
                        wscanw(wn, "%s", &str);
                        foo = atoi(str);
                    } else {
                        foo = ai_r;
                    }

                    if (!raise(&player_list[index], &pot, foo))
                        goto ch;

                    total += pot;

                    reset_raised(player_list);
                    player_list[index].raised = 1;
                    reset_call(player_list);
                    player_list[index].call = 1;
                    werase(wn);
                    break;
                case 'q':
                    *quit = 1;
                    save(player_list, end, count, index, r, pot, playerCard, riverCard, total, show, str);
                    return;
                default:
                    goto ch;
            }
            /* Player index wrap-around */

            index = (index < (playerNum - 1)) ? index + 1 : 0;

            //if (player_list[index].raised)
            //   break;

            werase(wn);
        }

        for (int a = 0; a < playerNum; a++)
            player_list[a].bet = 0;

        r++;
        pot = (r > 0) ? 0 : 2;
        reset_call(player_list);
        reset_raised(player_list);

        riverCard = (r == 1) ? 3 : riverCard + 1;
    }

    for (int a = 0; a < playerNum; a++) {
        if (player_list[a].fold) {
            player_list[a].rank = 0;
        } else {
            for (int b = 2; b < 7; b++)
                player_list[a].hand[b] = table_card[b - 2];
            player_list[a].rank = hand_rank(player_list[a].hand, 7);
        }
    }
    Player dump[4];
    for (int a = 0; a < playerNum; a++)
        dump[a] = player_list[a];
    selection_sort(dump, playerNum);

    player_list[atoi(dump[playerNum - 1].name)].money += total;
    printw("Player %s: %i\n", dump[playerNum - 1].name, dump[playerNum - 1].money);

    if (show) {
        for (int b = 0; b < playerNum; b++) {
            if (player_list[b].fold) {
                printCard(wn, playerCard, player_list[b].hand, b, 0, 0, player_list);
            } else {
                printCard(wn, playerCard, player_list[b].hand, b, 1, 0, player_list);
            }
        }

        printCard(wn, riverCard - 1, table_card, 4, 1, -1, NULL);

        for (int a = (playerNum - 1); a > -1 ; a--)
            mvprintw(49 - a, 0, "Player %s: %i. %s\n", dump[a].name, dump[a].rank, hand_name(dump[a].rank));
    } else {
        for (int b = 0; b < playerNum; b++) {
            printCard(wn, playerCard, player_list[b].hand, b, 0, 0, player_list);
        }
    }

    printw("Player %s win !\n", dump[playerNum - 1].name);
    total = 0;
    wgetch(wn);
}


void drawMenu(WINDOW* wn_a) {
    int quit = 0;
    int index = 0;
    int choice = 0;
    int loaded = 0;
    struct save s;
    card deck[52], table_card[5];
    Player player_list[4];
    start_color();
    while (1) {
        switch(choice) {

            case KEY_UP:
                if (index == 0)
                    break;
                index--;
                break;

            case KEY_DOWN:
                if (index == 3)
                    break;
                index++;
                break;

            case '\n':

                switch (index) {
                    case 0:
                        werase(wn_a);
                        choosePlayerNum(wn_a);
                        init_name_money(player_list, playerNum);

                        while (player_list[0].money >= 2 && !quit) {
                            init_card(deck);

                            for (int a = 0; a < playerNum; a++) {
                                if (player_list[a].money == 0) {
                                    playerNum--;
                                }
                            }

                            init_player(player_list, playerNum, deck, table_card);
                            gm:
                            game(wn_a, player_list, table_card, &quit, s, loaded);
                            werase(wn_a);
                        }
                        break;
                    case 1:
                        if (quit) {
                            s = load();
                            loaded = 1;
                            werase(wn_a);
                            goto gm;
                        } else {
                            break;
                        }
                    case 2:
                        werase(wn_a);
                        viewCredit(wn_a);
                        break;

                    case 3:
                        werase(wn_a);
                        mvwprintw(wn_a, 4, 75 , "Are you sure ? Y/N");

                    ret:;
                        int exitChoice = wgetch(wn_a);
                        switch(exitChoice) {
                            case 'Y':
                            case 'y':
                                werase(wn_a);
                                mvwprintw(wn_a, 3, 75, "Goodbye!\n");
                                move(4,75);
                                wprintw(wn_a,"Press any key...");
                                wgetch(wn_a);
                                echo();
                                delwin(wn_a);
                                endwin();
                                exit(0);
                            case 'N':
                            case 'n':
                                werase(wn_a);
                                break;
                            default:
                                goto ret;
                        }
                }
                break;
        }
        char gameName[] = "Poker Game";
        char gameType[] = "Texas Hold'em";

        attron( A_BOLD | A_UNDERLINE);
        move(7,75);
        addstr(gameName);
        move(8 ,75);
        addstr(gameType);
        attroff(A_BOLD | A_UNDERLINE);
        for (int foo = 0; foo < 4; foo++) {
            if (foo == index) {
                attron(A_REVERSE);
                if (foo == 0 && start == 1) {
                    mvwprintw(wn_a, foo + 15, 0, "%s", menuOpt[2]);
                } else {
                    mvwprintw(wn_a, foo + 15, 75, "%s", menuOpt[foo]);
                }
            } else {
                if (foo == 0 && start == 1) {
                    mvwprintw(wn_a, foo + 15, 0, "%s", menuOpt[2]);
                } else {
                    mvwprintw(wn_a, foo + 15, 75, "%s", menuOpt[foo]);
                }
            }
            attroff(A_REVERSE);
        }
        choice = wgetch(wn_a);
    }
}


int main(void) {

    srand(time(NULL));
    initscr();
    noecho();
    keypad(stdscr, TRUE);

    drawMenu(stdscr);

    return 0;
}




